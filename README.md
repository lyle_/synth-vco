# Voltage Controlled Oscillator

version 0.1

This is a design for a voltage controlled oscillator (VCO). My focus is on the
[Eurorack](https://en.wikipedia.org/wiki/Eurorack) format but it could easily be
adapted to other formats.

Most inspiration was draw from Moritz Klein's excellent [video series](https://www.youtube.com/watch?v=QBatvo8bCa4&list=PLHeL0JWdJLvTuGCyC3qvx0RM39YvopVQN) on
building a VCO.


## Current Features

* Waveforms: sawtooth, square/pulse with duty cycle knob
* Coarse and fine tuning knobs
* CV input
* Modest attempt at temperature stability using NTC thermistors and

## Schematic

Schematic done in [KiCad](https://www.kicad.org/).

![](images/VCO.svg)

## Stripboard Layout

Stripboard layout (`VCO.diy`) done in [DIY Layout Creator](https://bancika.github.io/diy-layout-creator/).

![](images/stripboard.png)


## Debugging
TP1 - unamplified base saw wave
TP2 - buffered saw
TP3 - buffered saw, AC coupled
TP4 - amplified saw, amplitude determines duty cycle of U2B stage
U1B pin 7 - amplified saw out (DC)

## TODO

- [ ] Triangle out
- [ ] Ramp out
- [ ] PWM input
- [ ] Reverse power protection
- [ ] In/out protection
- [ ] Bypass caps?
